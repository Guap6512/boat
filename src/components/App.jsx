import React, { Component } from 'react';
import './App.css';
import './Person/Person';
import Person from "./Person/Person";

class App extends Component {

    state = {
        activePerson: {
            id: 0,
            list: "",
            set: false
        },
        onBoatList: [
            { 'id': 0, "name": "peter", "list": "onBoatList", "weight": 131 },
            { 'id': 1, "name": "kristen", "list": "onBoatList", "weight": 125 },
            { 'id': 2, "name": "marian", "list": "onBoatList", "weight": 115 },
            { 'id': 3, "name": "simin", "list": "onBoatList", "weight": 148 },
            { 'id': 4, "name": "janet", "list": "onBoatList", "weight": 86 },
            { 'id': 5, "name": "shannon", "list": "onBoatList", "weight": 218 },
            { 'id': 6, "name": "jeanne", "list": "onBoatList", "weight": 197 },
            { 'id': 7, "name": "deadra", "list": "onBoatList", "weight": 165 },
            { 'id': 8, "name": "ginger", "list": "onBoatList", "weight": 140 },
            { 'id': 9, "name": "lesley", "list": "onBoatList", "weight": 120 },
            { 'id': 10, "name": "adriana", "list": "onBoatList", "weight": 163 },
            { 'id': 11, "name": "jen", "list": "onBoatList", "weight": 136 },
            { 'id': 12, "name": "tess", "list": "onBoatList", "weight": 130 },
            { 'id': 13, "name": "jeffC", "list": "onBoatList", "weight": 191 },
            { 'id': 14, "name": "gabriel", "list": "onBoatList", "weight": 139 },
            { 'id': 15, "name": "arun", "list": "onBoatList", "weight": 166 },
            { 'id': 16, "name": "jamesK", "list": "onBoatList", "weight": 159 },
            { 'id': 17, "name": "eli", "list": "onBoatList", "weight": 152 },
            { 'id': 18, "name": "aj", "list": "onBoatList", "weight": 177 },
            { 'id': 19, "name": "taylor", "list": "onBoatList", "weight": 160 }],

        outOfBoatList: [
            { 'id': 0, "name": "bird", "list": "outOfBoatList", "weight": 170 },
            { 'id': 1, "name": "emil", "list": "outOfBoatList", "weight": 164 },
            { 'id': 2, "name": "caesar", "list": "outOfBoatList", "weight": 203 },
        ]
    };

    componentDidMount() {
        this.togglePerson = this.togglePerson.bind(this);
    }

    resetActivePerson() {
        this.setState({
            activePerson: {
                id: 0,
                list: "",
                set: false
            }
        })
    }

    handleAddingToOutOfBoat() {
        let newEmptyPerson = {
            id: this.state.activePerson.id,
            list: this.state.activePerson.list,
            name: ""
        };
        let personToMove = this.state.onBoatList[this.state.activePerson.id];
        let newOutList = this.state.outOfBoatList;
        let newOnList = this.state.onBoatList;

        personToMove.list = "outOfBoatList";
        personToMove.id = newOutList.length + 1;
        newOutList.push(personToMove);
        newOnList[this.state.activePerson.id] = newEmptyPerson;
        newOnList.sort((onePerson, anotherPerson) => {
            return onePerson.id - anotherPerson.id;
        });

        this.setState({
            onBoatList: newOnList,
            outOfBoatList: newOutList
        });
        this.resetActivePerson();
        this.cleanOutOfBoatList();
    }

    togglePerson(idx, personList) {
        if(idx === -1 && !this.state.activePerson.set)
        {
            return;
        }
        console.log("Idx:", idx);
        console.log("List:", personList);
        if(!this.state.activePerson.set) {
            let newActivePerson = this.state.onBoatList[idx];
            this.setState({
                activePerson: {
                    id: newActivePerson.id,
                    list: personList,
                    set: true
                }
            })
        }
        else {
            if(personList === "outOfBoatList" && this.state.activePerson.list === "outOfBoatList") {
                this.resetActivePerson();
                return;
            }
            if(idx === -1) {
                this.handleAddingToOutOfBoat();
                return;
            }

            let newActivePersonList = this.state[this.state.activePerson.list];
            let newRecipientList = this.state[personList];
            console.log(newActivePersonList);
            console.log(newRecipientList);

            let activePersonModel = newActivePersonList[this.state.activePerson.id];
            let recipientPersonModel = newRecipientList[idx];

            console.log("Moving:", activePersonModel);
            console.log("To:", recipientPersonModel);

            let tmpIdHolder = recipientPersonModel.id;
            let tmpListHolder = recipientPersonModel.list;
            recipientPersonModel.id = activePersonModel.id;
            recipientPersonModel.list = activePersonModel.list;
            activePersonModel.id = tmpIdHolder;
            activePersonModel.list = tmpListHolder;

            let newState = this.state;

            if(this.state.activePerson.list === personList) {
                newActivePersonList[recipientPersonModel.id] = activePersonModel;
                newActivePersonList[activePersonModel.id] = recipientPersonModel;
                newActivePersonList.sort((onePerson, anotherPerson) => {
                    return onePerson.id - anotherPerson.id;
                });

                newState[personList] = newActivePersonList;
            } else {
                console.log("Active name:", activePersonModel.name);
                console.log("Active list:", this.state.activePerson.list);
                console.log("Recipient name:", recipientPersonModel.name);
                console.log("Recipient list:", personList);

                newActivePersonList[recipientPersonModel.id] = recipientPersonModel;
                newRecipientList[activePersonModel.id] = activePersonModel;

                console.log(newRecipientList);
                console.log(newActivePersonList);

                if(personList === "onBoatList") {   //personList name describes the list newRecipientList
                    newRecipientList.sort((onePerson, anotherPerson) => {
                        return onePerson.id - anotherPerson.id;
                    });
                } else {                            //We sort only one list, the one that is boat
                    newActivePersonList.sort((onePerson, anotherPerson) => {
                        return onePerson.id - anotherPerson.id;
                    });
                }
                newState[personList] = newRecipientList;
                newState[this.state.activePerson.list] = newActivePersonList;
            }
            this.setState(newState);
            this.resetActivePerson();
            this.cleanOutOfBoatList();
        }
    }

    cleanOutOfBoatList() {
        debugger;
        let newOutList = this.state.outOfBoatList.filter((person) => {
            return !(person.name === "");
        });
        newOutList.forEach((person, idx, thisArray) => {
            person.id = idx;
            thisArray[idx] = person;
        });
        this.setState({
            outOfBoatList: newOutList
        })
    }

    render() {
        let emptyData = {
            id: -1,
            list: "outOfBoatList"
        };
        return (
            <div className="container">
                <header className="header">
                    <h1>Boat</h1>
                </header>
                <div className="boat-container">
                    <div className="person-block-left">
                    {this.state.onBoatList.map((person, idx) => {
                        return (
                            <Person data={person} activePerson={this.state.activePerson} togglePerson={this.togglePerson.bind(this)}/>
                        )
                    })}
                    </div>
                    <div className="person-block-right">
                        {this.state.outOfBoatList.map((person, idx) => {
                            return (
                                <Person data={person} activePerson={this.state.activePerson} togglePerson={this.togglePerson.bind(this)}/>
                            )
                        })}
                        <Person data={emptyData} activePerson={this.state.activePerson} togglePerson={this.togglePerson.bind(this)}/>
                    </div>
                </div>
            </div>
        );
  }
}

export default App;
