import React, { Component } from 'react';
import './Person.css';

class Person extends Component {

    state = {
        active: false
    };

    componentDidMount() {
        console.log("Active person:", this.props.activePerson);
        if(this.props.activePerson.id === this.props.data.id && this.props.activePerson.set){
            this.setState({active: true});
        }
    }

    componentWillReceiveProps(nextProps) {
        if(nextProps.activePerson.set && nextProps.activePerson.id === nextProps.data.id
            && nextProps.activePerson.list === nextProps.data.list) {
            this.setState({active: true});
        } else {
            this.setState({active: false});
        }
    }

    render() {
        return(
            <div
                className={this.state.active ? "person-item active" : "person-item"}
                onClick={() => {this.props.togglePerson(this.props.data.id, this.props.data.list);}}>
                {this.props.data.name}<br/>
                {this.props.data.name !== "" ? this.props.data.id : "Empty"}
            </div>
        )
    }

}

export default Person;